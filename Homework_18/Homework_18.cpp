// Homework_18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>


using namespace std;

class  Player
{
public:
    char PName[90] = "somename";
    int PScore = 0;

    
    void SetPlayerData()
    {
        
        cin >> PName >> PScore;
    }
    
    void GetPlayerData()
    {
        cout << PName << "\t" << PScore << endl;
    }

};

int main()
{
      
    int size = 0;
    
	cout << "Enter number of players: " << endl;
	cin >> size;

    Player* arr = new Player[size];
   
    for (int i=0; i < size; i++)
    {
        
        cout << "Enter " << i+1 << " name and score" << endl;
        arr[i].SetPlayerData();
        
    }

    cout << endl;
    cout << "Values are: " << endl;
    for (int i = 0; i < size; i++)
	{
	    arr[i].GetPlayerData();
        
	}
    
	for (int i = 0; i < size; i++)
	{
		for (int j = size - 1; j > i; j--)
		{
			if ((arr[j].PScore) > (arr[j - 1].PScore))
			{
				swap(arr[j], arr[j - 1]);
			}
		}
	}

	cout << endl;
	cout << "sorted tab:" << endl;

	for (int i = 0; i < size; ++i)
	{
		arr[i].GetPlayerData();
	}
	delete[] arr;
}



